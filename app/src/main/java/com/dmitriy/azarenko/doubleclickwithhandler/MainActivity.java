package com.dmitriy.azarenko.doubleclickwithhandler;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button clicker;
    int nums = 0;


    Handler h = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1){

                nums++;
                h.sendEmptyMessageDelayed(1,500);
            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        clicker = (Button) findViewById(R.id.button);

        clicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                h.sendEmptyMessageDelayed(1,500);

                if (nums == 0){
                    Toast.makeText(getApplicationContext(), "Click!", Toast.LENGTH_SHORT).show();

                }else if (nums == 1){
                    Toast.makeText(getApplicationContext(), "Double click!", Toast.LENGTH_SHORT).show();


                }else if (nums==2){
                    Toast.makeText(getApplicationContext(), "Triple!", Toast.LENGTH_SHORT).show();

                }else if (nums==3){
                    Toast.makeText(getApplicationContext(), "Four!!", Toast.LENGTH_SHORT).show();
                }
                nums = 0;

            }
        });

    }
}
